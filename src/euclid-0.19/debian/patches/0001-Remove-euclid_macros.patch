From d0a3d71caa2bf5e4b3ca9e9d1ea1b3ca15e67271 Mon Sep 17 00:00:00 2001
From: Nicolas Silva <nical@fastmail.com>
Date: Wed, 19 Jun 2019 13:43:15 -0700
Subject: [PATCH] Remove euclid_macros.

This makes euclid much faster to build with default features.
---
 Cargo.toml              |   3 +-
 macros/Cargo.toml       |  15 ---
 macros/LICENSE-APACHE   | 201 --------------------------------
 macros/LICENSE-MIT      |  25 ----
 macros/euclid_matrix.rs | 247 ----------------------------------------
 macros/lib.rs           |  16 ---
 src/homogen.rs          |  62 +++++++++-
 src/lib.rs              |   2 -
 src/point.rs            | 116 ++++++++++++++++++-
 src/rotation.rs         | 119 ++++++++++++++++++-
 src/side_offsets.rs     |  66 ++++++++++-
 src/size.rs             | 116 ++++++++++++++++++-
 src/transform2d.rs      |  85 +++++++++++++-
 src/transform3d.rs      | 118 ++++++++++++++++++-
 src/translation.rs      | 116 ++++++++++++++++++-
 src/vector.rs           | 116 ++++++++++++++++++-
 16 files changed, 901 insertions(+), 522 deletions(-)
 delete mode 100644 macros/Cargo.toml
 delete mode 100644 macros/LICENSE-APACHE
 delete mode 100644 macros/LICENSE-MIT
 delete mode 100644 macros/euclid_matrix.rs
 delete mode 100644 macros/lib.rs

diff --git a/Cargo.toml b/Cargo.toml
index 146e12a..57b86e3 100644
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -20,8 +20,6 @@
 categories = ["science"]
 license = "MIT / Apache-2.0"
 repository = "https://github.com/servo/euclid"
-[dependencies.euclid_macros]
-version = "0.1"
 
 [dependencies.mint]
 version = "0.5.1"
diff --git a/src/homogen.rs b/src/homogen.rs
index 29a5073..2229115 100644
--- a/src/homogen.rs
+++ b/src/homogen.rs
@@ -15,10 +15,13 @@ use num::{One, Zero};
 use core::fmt;
 use core::marker::PhantomData;
 use core::ops::Div;
+use core::cmp::{Eq, PartialEq};
+use core::hash::{Hash};
+#[cfg(feature = "serde")]
+use serde;
 
 
 /// Homogeneous vector in 3D space.
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct HomogeneousVector<T, U> {
     pub x: T,
@@ -29,6 +32,63 @@ pub struct HomogeneousVector<T, U> {
     pub _unit: PhantomData<U>,
 }
 
+impl<T: Copy, U> Copy for HomogeneousVector<T, U> {}
+
+impl<T: Clone, U> Clone for HomogeneousVector<T, U> {
+    fn clone(&self) -> Self {
+        HomogeneousVector {
+            x: self.x.clone(),
+            y: self.y.clone(),
+            z: self.z.clone(),
+            w: self.w.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, U> serde::Deserialize<'de> for HomogeneousVector<T, U>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (x, y, z, w) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(HomogeneousVector { x, y, z, w, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, U> serde::Serialize for HomogeneousVector<T, U>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.x, &self.y, &self.z, &self.w).serialize(serializer)
+    }
+}
+
+impl<T, U> Eq for HomogeneousVector<T, U> where T: Eq {}
+
+impl<T, U> PartialEq for HomogeneousVector<T, U>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.x == other.x && self.y == other.y && self.z == other.z && self.w == other.w
+    }
+}
+
+impl<T, U> Hash for HomogeneousVector<T, U>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.x.hash(h);
+        self.y.hash(h);
+        self.z.hash(h);
+        self.w.hash(h);
+    }
+}
 
 impl<T, U> HomogeneousVector<T, U> {
     /// Constructor taking scalar values directly.
diff --git a/src/lib.rs b/src/lib.rs
index f604ab8..cc4ab6a 100644
--- a/src/lib.rs
+++ b/src/lib.rs
@@ -62,8 +62,6 @@ extern crate serde;
 
 #[cfg(feature = "mint")]
 pub extern crate mint;
-#[macro_use]
-extern crate euclid_macros;
 extern crate num_traits;
 #[cfg(test)]
 extern crate rand;
diff --git a/src/point.rs b/src/point.rs
index dc8debb..f77eb56 100644
--- a/src/point.rs
+++ b/src/point.rs
@@ -20,9 +20,12 @@ use vector::{TypedVector2D, TypedVector3D, vec2, vec3};
 use core::fmt;
 use core::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign};
 use core::marker::PhantomData;
+use core::cmp::{Eq, PartialEq};
+use core::hash::{Hash};
+#[cfg(feature = "serde")]
+use serde;
 
 /// A 2d Point tagged with a unit.
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedPoint2D<T, U> {
     pub x: T,
@@ -31,6 +34,60 @@ pub struct TypedPoint2D<T, U> {
     pub _unit: PhantomData<U>,
 }
 
+impl<T: Copy, U> Copy for TypedPoint2D<T, U> {}
+
+impl<T: Clone, U> Clone for TypedPoint2D<T, U> {
+    fn clone(&self) -> Self {
+        TypedPoint2D {
+            x: self.x.clone(),
+            y: self.y.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, U> serde::Deserialize<'de> for TypedPoint2D<T, U>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (x, y) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedPoint2D { x, y, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, U> serde::Serialize for TypedPoint2D<T, U>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.x, &self.y).serialize(serializer)
+    }
+}
+
+impl<T, U> Eq for TypedPoint2D<T, U> where T: Eq {}
+
+impl<T, U> PartialEq for TypedPoint2D<T, U>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.x == other.x && self.y == other.y
+    }
+}
+
+impl<T, U> Hash for TypedPoint2D<T, U>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.x.hash(h);
+        self.y.hash(h);
+    }
+}
+
 mint_vec!(TypedPoint2D[x, y] = Point2);
 
 /// Default 2d point type with no unit.
@@ -435,7 +492,6 @@ impl<T: Copy, U> From<(T, T)> for TypedPoint2D<T, U> {
 }
 
 /// A 3d Point tagged with a unit.
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedPoint3D<T, U> {
     pub x: T,
@@ -447,6 +503,62 @@ pub struct TypedPoint3D<T, U> {
 
 mint_vec!(TypedPoint3D[x, y, z] = Point3);
 
+impl<T: Copy, U> Copy for TypedPoint3D<T, U> {}
+
+impl<T: Clone, U> Clone for TypedPoint3D<T, U> {
+    fn clone(&self) -> Self {
+        TypedPoint3D {
+            x: self.x.clone(),
+            y: self.y.clone(),
+            z: self.z.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, U> serde::Deserialize<'de> for TypedPoint3D<T, U>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (x, y, z) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedPoint3D { x, y, z, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, U> serde::Serialize for TypedPoint3D<T, U>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.x, &self.y, &self.z).serialize(serializer)
+    }
+}
+
+impl<T, U> Eq for TypedPoint3D<T, U> where T: Eq {}
+
+impl<T, U> PartialEq for TypedPoint3D<T, U>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.x == other.x && self.y == other.y && self.z == other.z
+    }
+}
+
+impl<T, U> Hash for TypedPoint3D<T, U>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.x.hash(h);
+        self.y.hash(h);
+        self.z.hash(h);
+    }
+}
+
 /// Default 3d point type with no unit.
 ///
 /// `Point3D` provides the same methods as `TypedPoint3D`.
diff --git a/src/rotation.rs b/src/rotation.rs
index 29f021f..a213c70 100644
--- a/src/rotation.rs
+++ b/src/rotation.rs
@@ -12,9 +12,13 @@ use num_traits::{Float, FloatConst, One, Zero, NumCast};
 use core::fmt;
 use core::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Rem, Sub, SubAssign};
 use core::marker::PhantomData;
+use core::cmp::{Eq, PartialEq};
+use core::hash::{Hash};
 use trig::Trig;
 use {TypedPoint2D, TypedPoint3D, TypedVector2D, TypedVector3D, Vector3D, point2, point3, vec3};
 use {TypedTransform2D, TypedTransform3D, UnknownUnit};
+#[cfg(feature = "serde")]
+use serde;
 
 /// An angle in radians
 #[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Hash)]
@@ -186,7 +190,6 @@ impl<T: Neg<Output = T>> Neg for Angle<T> {
 }
 
 /// A transform that can represent rotations in 2d, represented as an angle in radians.
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedRotation2D<T, Src, Dst> {
     pub angle : T,
@@ -197,6 +200,58 @@ pub struct TypedRotation2D<T, Src, Dst> {
 /// The default 2d rotation type with no units.
 pub type Rotation2D<T> = TypedRotation2D<T, UnknownUnit, UnknownUnit>;
 
+impl<T: Copy, Src, Dst> Copy for TypedRotation2D<T, Src, Dst> {}
+
+impl<T: Clone, Src, Dst> Clone for TypedRotation2D<T, Src, Dst> {
+    fn clone(&self) -> Self {
+        TypedRotation2D {
+            angle: self.angle.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, Src, Dst> serde::Deserialize<'de> for TypedRotation2D<T, Src, Dst>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (angle,) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedRotation2D { angle, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, Src, Dst> serde::Serialize for TypedRotation2D<T, Src, Dst>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.angle,).serialize(serializer)
+    }
+}
+
+impl<T, Src, Dst> Eq for TypedRotation2D<T, Src, Dst> where T: Eq {}
+
+impl<T, Src, Dst> PartialEq for TypedRotation2D<T, Src, Dst>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.angle == other.angle
+    }
+}
+
+impl<T, Src, Dst> Hash for TypedRotation2D<T, Src, Dst>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.angle.hash(h);
+    }
+}
+
 impl<T, Src, Dst> TypedRotation2D<T, Src, Dst> {
     #[inline]
     /// Creates a rotation from an angle in radians.
@@ -322,7 +377,6 @@ where
 /// Some people use the `x, y, z, w` (or `w, x, y, z`) notations. The equivalence is
 /// as follows: `x -> i`, `y -> j`, `z -> k`, `w -> r`.
 /// The memory layout of this type corresponds to the `x, y, z, w` notation
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedRotation3D<T, Src, Dst> {
     /// Component multiplied by the imaginary number `i`.
@@ -340,6 +394,67 @@ pub struct TypedRotation3D<T, Src, Dst> {
 /// The default 3d rotation type with no units.
 pub type Rotation3D<T> = TypedRotation3D<T, UnknownUnit, UnknownUnit>;
 
+impl<T: Copy, Src, Dst> Copy for TypedRotation3D<T, Src, Dst> {}
+
+impl<T: Clone, Src, Dst> Clone for TypedRotation3D<T, Src, Dst> {
+    fn clone(&self) -> Self {
+        TypedRotation3D {
+            i: self.i.clone(),
+            j: self.j.clone(),
+            k: self.k.clone(),
+            r: self.r.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, Src, Dst> serde::Deserialize<'de> for TypedRotation3D<T, Src, Dst>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (i, j, k, r) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedRotation3D { i, j, k, r, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, Src, Dst> serde::Serialize for TypedRotation3D<T, Src, Dst>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.i, &self.j, &self.k, &self.r).serialize(serializer)
+    }
+}
+
+impl<T, Src, Dst> Eq for TypedRotation3D<T, Src, Dst> where T: Eq {}
+
+impl<T, Src, Dst> PartialEq for TypedRotation3D<T, Src, Dst>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.i == other.i &&
+            self.j == other.j &&
+            self.k == other.k &&
+            self.r == other.r
+    }
+}
+
+impl<T, Src, Dst> Hash for TypedRotation3D<T, Src, Dst>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.i.hash(h);
+        self.j.hash(h);
+        self.k.hash(h);
+        self.r.hash(h);
+    }
+}
+
 impl<T, Src, Dst> TypedRotation3D<T, Src, Dst> {
     /// Creates a rotation around from a quaternion representation.
     ///
diff --git a/src/side_offsets.rs b/src/side_offsets.rs
index fb3d76c..aa2f3a7 100644
--- a/src/side_offsets.rs
+++ b/src/side_offsets.rs
@@ -16,10 +16,13 @@ use num::Zero;
 use core::fmt;
 use core::ops::Add;
 use core::marker::PhantomData;
+use core::cmp::{Eq, PartialEq};
+use core::hash::{Hash};
+#[cfg(feature = "serde")]
+use serde;
 
 /// A group of 2D side offsets, which correspond to top/left/bottom/right for borders, padding,
 /// and margins in CSS, optionally tagged with a unit.
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedSideOffsets2D<T, U> {
     pub top: T,
@@ -30,6 +33,67 @@ pub struct TypedSideOffsets2D<T, U> {
     pub _unit: PhantomData<U>,
 }
 
+impl<T: Copy, U> Copy for TypedSideOffsets2D<T, U> {}
+
+impl<T: Clone, U> Clone for TypedSideOffsets2D<T, U> {
+    fn clone(&self) -> Self {
+        TypedSideOffsets2D {
+            top: self.top.clone(),
+            right: self.right.clone(),
+            bottom: self.bottom.clone(),
+            left: self.left.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, U> serde::Deserialize<'de> for TypedSideOffsets2D<T, U>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (top, right, bottom, left) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedSideOffsets2D { top, right, bottom, left, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, U> serde::Serialize for TypedSideOffsets2D<T, U>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.top, &self.right, &self.bottom, &self.left).serialize(serializer)
+    }
+}
+
+impl<T, U> Eq for TypedSideOffsets2D<T, U> where T: Eq {}
+
+impl<T, U> PartialEq for TypedSideOffsets2D<T, U>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.top == other.top &&
+            self.right == other.right &&
+            self.bottom == other.bottom &&
+            self.left == other.left
+    }
+}
+
+impl<T, U> Hash for TypedSideOffsets2D<T, U>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.top.hash(h);
+        self.right.hash(h);
+        self.bottom.hash(h);
+        self.left.hash(h);
+    }
+}
+
 impl<T: fmt::Debug, U> fmt::Debug for TypedSideOffsets2D<T, U> {
     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
         write!(
diff --git a/src/size.rs b/src/size.rs
index 9aae8f7..0101cc9 100644
--- a/src/size.rs
+++ b/src/size.rs
@@ -20,9 +20,12 @@ use num_traits::{Float, NumCast, Signed};
 use core::fmt;
 use core::ops::{Add, Div, Mul, Sub};
 use core::marker::PhantomData;
+use core::cmp::{Eq, PartialEq};
+use core::hash::{Hash};
+#[cfg(feature = "serde")]
+use serde;
 
 /// A 2d size tagged with a unit.
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedSize2D<T, U> {
     pub width: T,
@@ -31,6 +34,60 @@ pub struct TypedSize2D<T, U> {
     pub _unit: PhantomData<U>,
 }
 
+impl<T: Copy, U> Copy for TypedSize2D<T, U> {}
+
+impl<T: Clone, U> Clone for TypedSize2D<T, U> {
+    fn clone(&self) -> Self {
+        TypedSize2D {
+            width: self.width.clone(),
+            height: self.height.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, U> serde::Deserialize<'de> for TypedSize2D<T, U>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (width, height) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedSize2D { width, height, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, U> serde::Serialize for TypedSize2D<T, U>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.width, &self.height).serialize(serializer)
+    }
+}
+
+impl<T, U> Eq for TypedSize2D<T, U> where T: Eq {}
+
+impl<T, U> PartialEq for TypedSize2D<T, U>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.width == other.width && self.height == other.height
+    }
+}
+
+impl<T, U> Hash for TypedSize2D<T, U>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.width.hash(h);
+        self.height.hash(h);
+    }
+}
+
 /// Default 2d size type with no unit.
 ///
 /// `Size2D` provides the same methods as `TypedSize2D`.
@@ -478,7 +535,6 @@ mod size2d {
 }
 
 /// A 3d size tagged with a unit.
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedSize3D<T, U> {
     pub width: T,
@@ -488,6 +544,62 @@ pub struct TypedSize3D<T, U> {
     pub _unit: PhantomData<U>,
 }
 
+impl<T: Copy, U> Copy for TypedSize3D<T, U> {}
+
+impl<T: Clone, U> Clone for TypedSize3D<T, U> {
+    fn clone(&self) -> Self {
+        TypedSize3D {
+            width: self.width.clone(),
+            height: self.height.clone(),
+            depth: self.depth.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, U> serde::Deserialize<'de> for TypedSize3D<T, U>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (width, height, depth) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedSize3D { width, height, depth, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, U> serde::Serialize for TypedSize3D<T, U>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.width, &self.height, &self.depth).serialize(serializer)
+    }
+}
+
+impl<T, U> Eq for TypedSize3D<T, U> where T: Eq {}
+
+impl<T, U> PartialEq for TypedSize3D<T, U>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.width == other.width && self.height == other.height && self.depth == other.depth
+    }
+}
+
+impl<T, U> Hash for TypedSize3D<T, U>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.width.hash(h);
+        self.height.hash(h);
+        self.depth.hash(h);
+    }
+}
+
 /// Default 3d size type with no unit.
 ///
 /// `Size3D` provides the same methods as `TypedSize3D`.
diff --git a/src/transform2d.rs b/src/transform2d.rs
index 5e6af3d..14a52e9 100644
--- a/src/transform2d.rs
+++ b/src/transform2d.rs
@@ -19,10 +19,14 @@ use rect::TypedRect;
 use transform3d::TypedTransform3D;
 use core::ops::{Add, Mul, Div, Sub, Neg};
 use core::marker::PhantomData;
+use core::cmp::{Eq, PartialEq};
+use core::hash::{Hash};
 use approxeq::ApproxEq;
 use trig::Trig;
 use core::fmt;
 use num_traits::NumCast;
+#[cfg(feature = "serde")]
+use serde;
 
 /// A 2d transform stored as a 3 by 2 matrix in row-major order in memory.
 ///
@@ -40,7 +44,6 @@ use num_traits::NumCast;
 /// a vector is `v * T`. If your library is using column vectors, use `row_major` functions when you
 /// are asked for `column_major` representations and vice versa.
 #[repr(C)]
-#[derive(EuclidMatrix)]
 pub struct TypedTransform2D<T, Src, Dst> {
     pub m11: T, pub m12: T,
     pub m21: T, pub m22: T,
@@ -52,6 +55,86 @@ pub struct TypedTransform2D<T, Src, Dst> {
 /// The default 2d transform type with no units.
 pub type Transform2D<T> = TypedTransform2D<T, UnknownUnit, UnknownUnit>;
 
+impl<T: Copy, Src, Dst> Copy for TypedTransform2D<T, Src, Dst> {}
+
+impl<T: Clone, Src, Dst> Clone for TypedTransform2D<T, Src, Dst> {
+    fn clone(&self) -> Self {
+        TypedTransform2D {
+            m11: self.m11.clone(),
+            m12: self.m12.clone(),
+            m21: self.m21.clone(),
+            m22: self.m22.clone(),
+            m31: self.m31.clone(),
+            m32: self.m32.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, Src, Dst> serde::Deserialize<'de> for TypedTransform2D<T, Src, Dst>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (
+            m11, m12,
+            m21, m22,
+            m31, m32,
+        ) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedTransform2D {
+            m11, m12,
+            m21, m22,
+            m31, m32,
+            _unit: PhantomData
+        })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, Src, Dst> serde::Serialize for TypedTransform2D<T, Src, Dst>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (
+            &self.m11, &self.m12,
+            &self.m21, &self.m22,
+            &self.m31, &self.m32,
+        ).serialize(serializer)
+    }
+}
+
+impl<T, Src, Dst> Eq for TypedTransform2D<T, Src, Dst> where T: Eq {}
+
+impl<T, Src, Dst> PartialEq for TypedTransform2D<T, Src, Dst>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.m11 == other.m11 &&
+            self.m12 == other.m12 &&
+            self.m21 == other.m21 &&
+            self.m22 == other.m22 &&
+            self.m31 == other.m31 &&
+            self.m32 == other.m32
+    }
+}
+
+impl<T, Src, Dst> Hash for TypedTransform2D<T, Src, Dst>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.m11.hash(h);
+        self.m12.hash(h);
+        self.m21.hash(h);
+        self.m22.hash(h);
+        self.m31.hash(h);
+        self.m32.hash(h);
+    }
+}
+
 impl<T: Copy, Src, Dst> TypedTransform2D<T, Src, Dst> {
     /// Create a transform specifying its matrix elements in row-major order.
     ///
diff --git a/src/transform3d.rs b/src/transform3d.rs
index 13c6f71..d71a9fa 100644
--- a/src/transform3d.rs
+++ b/src/transform3d.rs
@@ -24,7 +24,11 @@ use num::{One, Zero};
 use core::ops::{Add, Mul, Sub, Div, Neg};
 use core::marker::PhantomData;
 use core::fmt;
+use core::cmp::{Eq, PartialEq};
+use core::hash::{Hash};
 use num_traits::NumCast;
+#[cfg(feature = "serde")]
+use serde;
 
 /// A 3d transform stored as a 4 by 4 matrix in row-major order in memory.
 ///
@@ -41,7 +45,6 @@ use num_traits::NumCast;
 /// These transforms are for working with _row vectors_, so the matrix math for transforming
 /// a vector is `v * T`. If your library is using column vectors, use `row_major` functions when you
 /// are asked for `column_major` representations and vice versa.
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedTransform3D<T, Src, Dst> {
     pub m11: T, pub m12: T, pub m13: T, pub m14: T,
@@ -55,6 +58,119 @@ pub struct TypedTransform3D<T, Src, Dst> {
 /// The default 3d transform type with no units.
 pub type Transform3D<T> = TypedTransform3D<T, UnknownUnit, UnknownUnit>;
 
+impl<T: Copy, Src, Dst> Copy for TypedTransform3D<T, Src, Dst> {}
+
+impl<T: Clone, Src, Dst> Clone for TypedTransform3D<T, Src, Dst> {
+    fn clone(&self) -> Self {
+        TypedTransform3D {
+            m11: self.m11.clone(),
+            m12: self.m12.clone(),
+            m13: self.m13.clone(),
+            m14: self.m14.clone(),
+            m21: self.m21.clone(),
+            m22: self.m22.clone(),
+            m23: self.m23.clone(),
+            m24: self.m24.clone(),
+            m31: self.m31.clone(),
+            m32: self.m32.clone(),
+            m33: self.m33.clone(),
+            m34: self.m34.clone(),
+            m41: self.m41.clone(),
+            m42: self.m42.clone(),
+            m43: self.m43.clone(),
+            m44: self.m44.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, Src, Dst> serde::Deserialize<'de> for TypedTransform3D<T, Src, Dst>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (
+            m11, m12, m13, m14,
+            m21, m22, m23, m24,
+            m31, m32, m33, m34,
+            m41, m42, m43, m44,
+        ) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedTransform3D {
+            m11, m12, m13, m14,
+            m21, m22, m23, m24,
+            m31, m32, m33, m34,
+            m41, m42, m43, m44,
+            _unit: PhantomData
+        })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, Src, Dst> serde::Serialize for TypedTransform3D<T, Src, Dst>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (
+            &self.m11, &self.m12, &self.m13, &self.m14,
+            &self.m21, &self.m22, &self.m23, &self.m24,
+            &self.m31, &self.m32, &self.m33, &self.m34,
+            &self.m41, &self.m42, &self.m43, &self.m44,
+        ).serialize(serializer)
+    }
+}
+
+impl<T, Src, Dst> Eq for TypedTransform3D<T, Src, Dst> where T: Eq {}
+
+impl<T, Src, Dst> PartialEq for TypedTransform3D<T, Src, Dst>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.m11 == other.m11 &&
+            self.m12 == other.m12 &&
+            self.m13 == other.m13 &&
+            self.m14 == other.m14 &&
+            self.m21 == other.m21 &&
+            self.m22 == other.m22 &&
+            self.m23 == other.m23 &&
+            self.m24 == other.m24 &&
+            self.m31 == other.m31 &&
+            self.m32 == other.m32 &&
+            self.m33 == other.m33 &&
+            self.m34 == other.m34 &&
+            self.m41 == other.m41 &&
+            self.m42 == other.m42 &&
+            self.m43 == other.m43 &&
+            self.m44 == other.m44
+    }
+}
+
+impl<T, Src, Dst> Hash for TypedTransform3D<T, Src, Dst>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.m11.hash(h);
+        self.m12.hash(h);
+        self.m13.hash(h);
+        self.m14.hash(h);
+        self.m21.hash(h);
+        self.m22.hash(h);
+        self.m23.hash(h);
+        self.m24.hash(h);
+        self.m31.hash(h);
+        self.m32.hash(h);
+        self.m33.hash(h);
+        self.m34.hash(h);
+        self.m41.hash(h);
+        self.m42.hash(h);
+        self.m43.hash(h);
+        self.m44.hash(h);
+    }
+}
+
 impl<T, Src, Dst> TypedTransform3D<T, Src, Dst> {
     /// Create a transform specifying its components in row-major order.
     ///
diff --git a/src/translation.rs b/src/translation.rs
index 9f3ca04..ed85c32 100644
--- a/src/translation.rs
+++ b/src/translation.rs
@@ -14,6 +14,10 @@ use trig::Trig;
 use core::ops::{Add, Sub, Neg, Mul, Div};
 use core::marker::PhantomData;
 use core::fmt;
+use core::cmp::{Eq, PartialEq};
+use core::hash::{Hash};
+#[cfg(feature = "serde")]
+use serde;
 
 /// A 2d transformation from a space to another that can only express translations.
 ///
@@ -35,7 +39,6 @@ use core::fmt;
 /// let p2: ChildPoint = scrolling.transform_point(&p1);
 /// ```
 ///
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedTranslation2D<T, Src, Dst> {
     pub x: T,
@@ -44,6 +47,60 @@ pub struct TypedTranslation2D<T, Src, Dst> {
     pub _unit: PhantomData<(Src, Dst)>,
 }
 
+impl<T: Copy, Src, Dst> Copy for TypedTranslation2D<T, Src, Dst> {}
+
+impl<T: Clone, Src, Dst> Clone for TypedTranslation2D<T, Src, Dst> {
+    fn clone(&self) -> Self {
+        TypedTranslation2D {
+            x: self.x.clone(),
+            y: self.y.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, Src, Dst> serde::Deserialize<'de> for TypedTranslation2D<T, Src, Dst>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (x, y) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedTranslation2D { x, y, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, Src, Dst> serde::Serialize for TypedTranslation2D<T, Src, Dst>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.x, &self.y).serialize(serializer)
+    }
+}
+
+impl<T, Src, Dst> Eq for TypedTranslation2D<T, Src, Dst> where T: Eq {}
+
+impl<T, Src, Dst> PartialEq for TypedTranslation2D<T, Src, Dst>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.x == other.x && self.y == other.y
+    }
+}
+
+impl<T, Src, Dst> Hash for TypedTranslation2D<T, Src, Dst>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.x.hash(h);
+        self.y.hash(h);
+    }
+}
+
 impl<T, Src, Dst> TypedTranslation2D<T, Src, Dst> {
     #[inline]
     pub fn new(x: T, y: T) -> Self {
@@ -239,7 +296,6 @@ where T: Copy + fmt::Debug {
 ///
 /// The main benefit of this type over a TypedVector3D is the ability to cast
 /// between a source and a destination spaces.
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedTranslation3D<T, Src, Dst> {
     pub x: T,
@@ -249,6 +305,62 @@ pub struct TypedTranslation3D<T, Src, Dst> {
     pub _unit: PhantomData<(Src, Dst)>,
 }
 
+impl<T: Copy, Src, Dst> Copy for TypedTranslation3D<T, Src, Dst> {}
+
+impl<T: Clone, Src, Dst> Clone for TypedTranslation3D<T, Src, Dst> {
+    fn clone(&self) -> Self {
+        TypedTranslation3D {
+            x: self.x.clone(),
+            y: self.y.clone(),
+            z: self.z.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, Src, Dst> serde::Deserialize<'de> for TypedTranslation3D<T, Src, Dst>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (x, y, z) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedTranslation3D { x, y, z, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, Src, Dst> serde::Serialize for TypedTranslation3D<T, Src, Dst>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.x, &self.y, &self.z).serialize(serializer)
+    }
+}
+
+impl<T, Src, Dst> Eq for TypedTranslation3D<T, Src, Dst> where T: Eq {}
+
+impl<T, Src, Dst> PartialEq for TypedTranslation3D<T, Src, Dst>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.x == other.x && self.y == other.y && self.z == other.z
+    }
+}
+
+impl<T, Src, Dst> Hash for TypedTranslation3D<T, Src, Dst>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.x.hash(h);
+        self.y.hash(h);
+        self.z.hash(h);
+    }
+}
+
 impl<T, Src, Dst> TypedTranslation3D<T, Src, Dst> {
     #[inline]
     pub fn new(x: T, y: T, z: T) -> Self {
diff --git a/src/vector.rs b/src/vector.rs
index ec87657..ec6eac0 100644
--- a/src/vector.rs
+++ b/src/vector.rs
@@ -24,9 +24,12 @@ use num_traits::{Float, NumCast, Signed};
 use core::fmt;
 use core::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};
 use core::marker::PhantomData;
+use core::cmp::{Eq, PartialEq};
+use core::hash::{Hash};
+#[cfg(feature = "serde")]
+use serde;
 
 /// A 2d Vector tagged with a unit.
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedVector2D<T, U> {
     pub x: T,
@@ -37,6 +40,60 @@ pub struct TypedVector2D<T, U> {
 
 mint_vec!(TypedVector2D[x, y] = Vector2);
 
+impl<T: Copy, U> Copy for TypedVector2D<T, U> {}
+
+impl<T: Clone, U> Clone for TypedVector2D<T, U> {
+    fn clone(&self) -> Self {
+        TypedVector2D {
+            x: self.x.clone(),
+            y: self.y.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, U> serde::Deserialize<'de> for TypedVector2D<T, U>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (x, y) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedVector2D { x, y, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, U> serde::Serialize for TypedVector2D<T, U>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.x, &self.y).serialize(serializer)
+    }
+}
+
+impl<T, U> Eq for TypedVector2D<T, U> where T: Eq {}
+
+impl<T, U> PartialEq for TypedVector2D<T, U>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.x == other.x && self.y == other.y
+    }
+}
+
+impl<T, U> Hash for TypedVector2D<T, U>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.x.hash(h);
+        self.y.hash(h);
+    }
+}
+
 /// Default 2d vector type with no unit.
 ///
 /// `Vector2D` provides the same methods as `TypedVector2D`.
@@ -521,7 +578,6 @@ where
 }
 
 /// A 3d Vector tagged with a unit.
-#[derive(EuclidMatrix)]
 #[repr(C)]
 pub struct TypedVector3D<T, U> {
     pub x: T,
@@ -533,6 +589,62 @@ pub struct TypedVector3D<T, U> {
 
 mint_vec!(TypedVector3D[x, y, z] = Vector3);
 
+impl<T: Copy, U> Copy for TypedVector3D<T, U> {}
+
+impl<T: Clone, U> Clone for TypedVector3D<T, U> {
+    fn clone(&self) -> Self {
+        TypedVector3D {
+            x: self.x.clone(),
+            y: self.y.clone(),
+            z: self.z.clone(),
+            _unit: PhantomData,
+        }
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<'de, T, U> serde::Deserialize<'de> for TypedVector3D<T, U>
+    where T: serde::Deserialize<'de>
+{
+    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
+        where D: serde::Deserializer<'de>
+    {
+        let (x, y, z) = try!(serde::Deserialize::deserialize(deserializer));
+        Ok(TypedVector3D { x, y, z, _unit: PhantomData })
+    }
+}
+
+#[cfg(feature = "serde")]
+impl<T, U> serde::Serialize for TypedVector3D<T, U>
+    where T: serde::Serialize
+{
+    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
+        where S: serde::Serializer
+    {
+        (&self.x, &self.y, &self.z).serialize(serializer)
+    }
+}
+
+impl<T, U> Eq for TypedVector3D<T, U> where T: Eq {}
+
+impl<T, U> PartialEq for TypedVector3D<T, U>
+    where T: PartialEq
+{
+    fn eq(&self, other: &Self) -> bool {
+        self.x == other.x && self.y == other.y && self.z == other.z
+    }
+}
+
+impl<T, U> Hash for TypedVector3D<T, U>
+    where T: Hash
+{
+    fn hash<H: ::core::hash::Hasher>(&self, h: &mut H) {
+        self.x.hash(h);
+        self.y.hash(h);
+        self.z.hash(h);
+    }
+}
+
 /// Default 3d vector type with no unit.
 ///
 /// `Vector3D` provides the same methods as `TypedVector3D`.
-- 
2.20.1

